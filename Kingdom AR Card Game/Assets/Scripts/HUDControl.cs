﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class HUDControl : MonoBehaviour
{
    public CardHandlerControl cardHandlerControl;
    public List<GameObject> objectsToEnable = new List<GameObject>();
    public List<GameObject> objectsToDisable = new List<GameObject>();
    public GameObject panel;

    //public event Action SetHUDObjectsActive;

    private void Start()
    {
        DisableHUDObjects();
        if (cardHandlerControl == null)
        {
            cardHandlerControl = GameObject.Find("Card Handler").GetComponent<CardHandlerControl>();    
        }

        if (cardHandlerControl != null)
        {
            cardHandlerControl.deckControl.DeckSetted += EnableHUDObjects;
        }
        
    }

    private void EnableHUDObjects()
    {
        for (int i = 0; i < objectsToEnable.Count; i++)
        {
            objectsToEnable[i].SetActive(true);
        }
    }

    public void DisableHUDObjects()
    {
        for (int i = 0; i < objectsToDisable.Count; i++)
        {
            objectsToEnable[i].SetActive(false);
        }
    }

    public void SetPanelActive()
    {
        panel.SetActive(true);
    }

    public void SetPanelInactive()
    {
        panel.SetActive(false);
    }
    
    
    
}
