﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class DeckControl : MonoBehaviour
{

    #region Fields
    
    public CardHandlerControl cardHandlerControl;
    public GameObject basicCard;
    public GameObject deckBase;
    public float spawnCardInterval = 0.2f;
    public float cardVerticalOffset = 0.1f;
    public List<GameObject> deck = new List<GameObject>();
    public List<GameObject> grave = new List<GameObject>();
    
    public event Action DeckSetted;
    //public event Action DeckLoading;
    public event Action<int> DeckLoaded;
    
    #endregion
    
    #region UnityCallbacks

    // Start is called before the first frame update
    void Start()
    {
        //playerControl.DeckLoading += SetDeck;
        //SetDeck();
    }
    
    #endregion
    
    #region Methods

    public void SetDeck()
    {
        int maxCards = 0;
        for (int i = 0; i < cardHandlerControl.agentInfo.deck.cards.Count; i++)
        {
            maxCards += cardHandlerControl.agentInfo.deck.cards[i].quantity;
        }
        StartCoroutine(LoadDeck(maxCards));
    }

    private IEnumerator LoadDeck(int maxCards)
    {
        for (int i = 0; i < maxCards; i++)
        {
            SpawnBasicCard(i);
            yield return new WaitForSeconds(spawnCardInterval);
        }
        cardHandlerControl.isDeckSetted = true;
        
        StartCoroutine(SetCardsSprite());
    }

    private IEnumerator SetCardsSprite()
    {
        int index;
        int quantity;
        int j;
        Sprite cardSprite;

        for (int i = 0; i < cardHandlerControl.agentInfo.deck.cards.Count; i++)
        {
            quantity = cardHandlerControl.agentInfo.deck.cards[i].quantity;
            cardSprite = cardHandlerControl.agentInfo.deck.cards[i].cardImage;
            j = 0;
            while (j < quantity)
            {
                index = Random.Range(0, deck.Count);
                CardControl.CardInfo actualCardInfo = deck[index].GetComponent<CardControl>().cardInfo;
                if (actualCardInfo.cardSprite == null)
                {
                    actualCardInfo.cardSprite = cardSprite;
                    actualCardInfo.rarity = cardHandlerControl.agentInfo.deck.cards[i].rarity;
                    j++;
                }
            }
            yield return null;
        }
        cardHandlerControl.canDraw = true;

        if (DeckSetted != null)
        {
            DeckSetted.Invoke();    
        }

        if (DeckLoaded != null)
        {
            DeckLoaded.Invoke(5);
        }
    }

    private void SpawnBasicCard(int index)
    {
        var cardPosition = new Vector3(deckBase.transform.localPosition.x, deckBase.transform.localPosition.y , deckBase.transform.localPosition.z - (cardVerticalOffset * (index + 1)));
        var cardRotation = deckBase.transform.rotation;
        var cardInstance = Instantiate(basicCard);
        var cardControl = cardInstance.GetComponent<CardControl>();
        cardInstance.transform.parent = transform;
        cardInstance.transform.localPosition = cardPosition;
        cardInstance.transform.rotation = cardRotation;
        cardInstance.layer = 10;
        cardControl.cardHandlerControl = cardHandlerControl;
        cardControl.CardPlayed += cardHandlerControl.CardPlayed;
        cardControl.StartDragging += cardHandlerControl.hudControl.SetPanelActive;
        cardControl.StopDragging += cardHandlerControl.hudControl.SetPanelInactive;
        deck.Add(cardInstance);
    }
    
    #endregion
}
