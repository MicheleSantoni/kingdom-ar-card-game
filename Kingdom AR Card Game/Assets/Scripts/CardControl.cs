﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CardControl : MonoBehaviour
{
    
    #region Fields
    
    [System.Serializable]
    public class CardInfo
    {
        public Sprite cardSprite;
        public DeckSO.Card.Rarity rarity;
        
    }
    public CardInfo cardInfo;
    public bool isOnHand;
    public float verticalOffset = 0.5f;
    public float distanceFromCamera = 5f;
    public AnimationCurve cardMovementCurve;
    [HideInInspector] public CardHandlerControl cardHandlerControl;
    [HideInInspector] public string onDragSortingLayer = "OnDrag";
    [HideInInspector] public string onHandSortingLayer = "OnHand";
    
    //public event Action ClickedOnCard;
    public event Action<int> CardPlayed;
    public event Action StartDragging;
    public event Action StopDragging;

    private SpriteRenderer spriteRenderer;
    private Vector3 basePosition;
    private Vector3 raisedPosition;
    private bool isOnTarget;
    private IEnumerator coroutine;
    
    
    #endregion
    
    #region UnityCallbacks

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnMouseEnter()
    {
        if (isOnHand && !cardHandlerControl.isDraggingCard)
        {
            //Debug.Log("raise");
            isOnTarget = false;
            if (coroutine != null)
            {
                StopCoroutine(coroutine);    
            }
            coroutine = MoveCard(raisedPosition);
            StartCoroutine(coroutine);
        }
    }

    private void OnMouseExit()
    {
        if (isOnHand && !cardHandlerControl.isDraggingCard)
        {
            isOnTarget = false;
            if (coroutine != null)
            {
                StopCoroutine(coroutine);    
            }
            coroutine = MoveCard(basePosition);
            StartCoroutine(coroutine);
        }
        
    }

    private void OnMouseDown()
    {
        if (isOnHand)
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);    
            }
            
            //Debug.Log("dragging");
            cardHandlerControl.isDraggingCard = true;
            if (StartDragging != null)
            {
                StartDragging.Invoke();    
            }
            coroutine = DragCardAround();
            StartCoroutine(coroutine);    
        }
    }

//    private void OnMouseDrag()
//    {
//        if (isOnHand)
//        {
//            StopCoroutine(coroutine);
//            Debug.Log("dragging");
//            playerControl.isDraggingCard = true;
//            DragCardAround();    
//        }
//    }

    private void OnMouseUp()
    {
        //Debug.Log("mouse up");
        if (coroutine != null)
        {
            StopCoroutine(coroutine);    
        }
        cardHandlerControl.isDraggingCard = false;
        if (StopDragging != null)
        {
            StopDragging.Invoke();    
        }
        ControlCardScreenPoint();
    }

    #endregion
    
    #region Methods

    private IEnumerator MoveCard(Vector3 targetPosition)
    {
        //Debug.Log("target " + targetPosition);
        float animCurvePosition = 0f;
        while (!isOnTarget)
        {
            animCurvePosition = Time.deltaTime;
            transform.localPosition = Vector3.Lerp(transform.localPosition, targetPosition,
                cardMovementCurve.Evaluate(animCurvePosition));

            if (transform.localPosition == targetPosition)
            {
                isOnTarget = true;
            }
            yield return null;    
        }
    }

    public void SetTargetPosition()
    {
        //Debug.Log("set target");
        basePosition = transform.localPosition;
        raisedPosition = basePosition + (transform.up * verticalOffset);
        isOnHand = true;
        isOnTarget = true;
    }

    private IEnumerator DragCardAround()
    {
        while (cardHandlerControl.isDraggingCard)
        {
            SetSortingLayer(onDragSortingLayer);
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, cardHandlerControl.playerCam.nearClipPlane + distanceFromCamera);
            Vector3 objPosition = cardHandlerControl.playerCam.ScreenToWorldPoint(mousePosition);
            transform.position = objPosition;
            
            yield return null;
        }
        
    }

    public void SetSortingLayer(string sortingLayerName)
    {
        spriteRenderer.sortingLayerName = sortingLayerName;
    }

    public void SetSortingOrder(int sortingOrder)
    {
        spriteRenderer.sortingOrder = sortingOrder;
    }

    private void ControlCardScreenPoint()
    {
        Vector2 screenPoint = cardHandlerControl.playerCam.WorldToScreenPoint(transform.position);
        if (screenPoint.y > Screen.height / 2f)
        {
            int index = cardHandlerControl.handControl.cardsOnHand.IndexOf(gameObject);
            CardPlayed.Invoke(index);
            Destroy(gameObject);
        }
        else
        {
            coroutine = MoveCard(basePosition);
            StartCoroutine(coroutine);
            SetSortingLayer(onHandSortingLayer);
        }
    }
    
    #endregion
}
