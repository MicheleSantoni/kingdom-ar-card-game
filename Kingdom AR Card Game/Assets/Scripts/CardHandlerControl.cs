﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CardHandlerControl : MonoBehaviour
{
    #region Fields
    
    public AgentInfoSO agentInfo;
    public HandControl handControl;
    public DeckControl deckControl;
    public HUDControl hudControl;
    [HideInInspector] public Camera mainCam;
    public Camera playerCam;
    public bool isDeckSetted;
    public bool canDraw;
    public bool isDraggingCard;
    public float drawnCardDelay = 0.2f;
    
    #endregion
    
    #region UnityCallbacks

    private void Awake()
    {
        if (deckControl != null)
        {
            deckControl.cardHandlerControl = this;
        }
        else
        {
            Debug.LogWarning("Deck value in PlayerControl is NULL!");
        }
        
        mainCam = Camera.main;
    }

    // Start is called before the first frame update
    void Start()
    {
        isDeckSetted = false;
        canDraw = false;
        isDraggingCard = false;
        deckControl.DeckLoaded += DrawCards;
        Sprite baseCardSprite = deckControl.basicCard.GetComponent<SpriteRenderer>().sprite;
        handControl.cardDinstance = (baseCardSprite.texture.width / baseCardSprite.pixelsPerUnit) / 2f;
    }

    #endregion
    
    #region Methods

    private void DrawCards(int quantity)
    {
        StartCoroutine(DrawnCardsDelayControl(quantity));
    }
    
    public void DrawACard()
    {
        if (deckControl.deck.Count > 0)
        {
            Debug.Log("drawed a card");
            GameObject drawnCard = deckControl.deck[deckControl.deck.Count - 1];
            var DrawnCardControl = drawnCard.GetComponent<CardControl>();
            deckControl.deck.RemoveAt(deckControl.deck.Count - 1);
            handControl.cardsOnHand.Add(drawnCard);
            drawnCard.transform.parent = handControl.transform;
            drawnCard.GetComponent<SpriteRenderer>().sprite = drawnCard.GetComponent<CardControl>().cardInfo.cardSprite;
            drawnCard.transform.localPosition = Vector3.zero;
            drawnCard.transform.localRotation = playerCam.transform.rotation;
            DrawnCardControl.SetTargetPosition();
            DrawnCardControl.SetSortingLayer(DrawnCardControl.onHandSortingLayer);
            handControl.SpacingCards();
        }
    }
    

    private IEnumerator DrawnCardsDelayControl(int quantity)
    {
        for (int i = 0; i < quantity; i++)
        {
            if (deckControl.deck.Count <= 0)
            {
                Debug.Log("cards finished");
                yield break;
            }
            DrawACard();
            yield return new WaitForSeconds(drawnCardDelay);
        }
    }

    public void CardPlayed(int index)
    {
        handControl.RemoveCardToHand(index);
        handControl.SpacingCards();
    }
    
    #endregion
}
