﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor.VersionControl;

public class AgentManagement : MonoBehaviour
{
    #region Fields
    
    [Title("Cards Lists")]
    [InfoBox("DON'T CHANGE THESE LISTS ON PLAY_MODE", InfoMessageType.Warning)]
    public List<DeckSO.Card> cardsOnDeck = new List<DeckSO.Card>();
    public List<DeckSO.Card> cardsOnHand = new List<DeckSO.Card>();
    public List<DeckSO.Card> cardsOnGraveyard = new List<DeckSO.Card>();
    
    [Space(10)]
    
    [Title("Stats")]
    public int health;
    public int attack;
    public int bonusAttack;
    public int defense;
    public int bonusDefense;
    public int speed;
    public int bonusSpeed;

    [Space(10)] 
    
    [Title("References")] 
    public GameObject cameraPoint;
    
    #endregion

    #region UnityCallbacks

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #endregion

    #region Methods

    public void InitStats(AgentInfoSO agentInfo)
    {
        health = agentInfo.maxHealth;
        attack = agentInfo.baseAttack;
        bonusAttack = 0;
        defense = agentInfo.baseDefense;
        bonusDefense = 0;
        speed = agentInfo.baseSpeed;
        bonusSpeed = 0;
    }

    #endregion
    
}
