﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManagement : MonoBehaviour
{
    #region Fields
    
    [System.Serializable]
    public class Agent
    {
        public AgentInfoSO agentInfo;
        public GameObject agentObject;

        public Agent(AgentInfoSO info, GameObject agentObj)
        {
            agentInfo = info;
            agentObject = agentObj;
        }
    }
    public List<Agent> players = new List<Agent>();
    public List<Agent> enemies = new List<Agent>();
    public Camera mainCam;
    public List<Agent> combatOrder = new List<Agent>();

    
    [HideInInspector] public int agentTurnIndex = 0;
    [HideInInspector] public HUDControl hudControl;
    [HideInInspector] public GameGenerator gameGenerator;
    
    public event Action InitiativeRolled;
    
    
    
    #endregion
    
    #region UnityCallbacks
    
    // Start is called before the first frame update
    void Start()
    {
        if (!mainCam)
            mainCam = Camera.main;
        if (!hudControl)
            hudControl = GameObject.Find("HUD").GetComponent<HUDControl>();
        if (!gameGenerator)
            gameGenerator = GetComponent<GameGenerator>();
        
        StartCoroutine(RollInitiative());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
    
    #endregion

    #region Methods

    private IEnumerator RollInitiative()
    {
        // decide the combat order
        Debug.Log("roll initiative");
        AgentManagement agentManagement;
        AgentManagement opponentManagement;
        int agentSpeed = 0;
        int opponentSpeed = 0;
        
        Debug.Log("players");
        // players first
        for (int i = 0; i < players.Count; i++)
        {
            Debug.Log(i);
            if (combatOrder.Count == 0)
            {
                combatOrder.Add(players[i]);
            }
            else if (combatOrder.Count > 0)
            {
                // check who has the highest speed
                agentManagement = players[i].agentObject.GetComponent<AgentManagement>();
                agentSpeed = agentManagement.speed + agentManagement.bonusSpeed;
                
                for (int j = 0; j < combatOrder.Count; j++)
                {
                    yield return null;
                    opponentManagement = combatOrder[j].agentObject.GetComponent<AgentManagement>();
                    opponentSpeed = opponentManagement.speed + opponentManagement.bonusSpeed;
                    if (agentSpeed > opponentSpeed)
                    {
                        combatOrder.Insert(j, players[i]);
                        break;
                    }
                    else if (agentSpeed == opponentSpeed)
                    {
                        int rnd = Random.Range(0, 100);
                        if (rnd < 50)
                        {
                            combatOrder.Insert(j, players[i]);
                            break;
                        }
                        else
                        {
                            combatOrder.Add(players[i]);
                            break;
                        }
                    }
                    else
                    {
                        combatOrder.Add(players[i]);
                        break;
                    }
                }
            }
        }
        
        Debug.Log("enemies");
        // enemies last
        for (int i = 0; i < enemies.Count; i++)
        {
            Debug.Log(i);
            if (combatOrder.Count == 0)
            {
                combatOrder.Add(enemies[i]);
            }
            else if (combatOrder.Count > 0)
            {
                // check who has the highest speed
                agentManagement = enemies[i].agentObject.GetComponent<AgentManagement>();
                agentSpeed = agentManagement.speed + agentManagement.bonusSpeed;
                
                for (int j = 0; j < combatOrder.Count; j++)
                {
                    yield return null;
                    opponentManagement = combatOrder[j].agentObject.GetComponent<AgentManagement>();
                    opponentSpeed = opponentManagement.speed + opponentManagement.bonusSpeed;
                    if (agentSpeed > opponentSpeed)
                    {
                        combatOrder.Insert(j, enemies[i]);
                        break;
                    }
                    else if (agentSpeed == opponentSpeed)
                    {
                        int rnd = Random.Range(0, 100);
                        if (rnd < 50)
                        {
                            combatOrder.Insert(j, enemies[i]);
                            break;
                        }
                        else
                        {
                            combatOrder.Add(enemies[i]);
                            break;
                        }
                    }
                    else
                    {
                        combatOrder.Add(enemies[i]);
                        break;
                    }
                }
            }
        }
        
        InitiativeRolled.Invoke();
    }

    #endregion
    
}
