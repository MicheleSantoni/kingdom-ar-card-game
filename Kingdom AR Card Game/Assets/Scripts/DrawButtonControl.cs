﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class DrawButtonControl : MonoBehaviour
{
    #region Fields
    
    [FormerlySerializedAs("playerControl")] public CardHandlerControl cardHandlerControl;
    
    private Button drawButton;
    
    #endregion

    #region UnityCallbacks
    
    private void Start()
    {
        if (cardHandlerControl == null)
        {
            cardHandlerControl = GameObject.Find("Card Handler").GetComponent<CardHandlerControl>();
        }
        drawButton = GetComponent<Button>();

        if (cardHandlerControl != null)
        {
            
            drawButton.onClick.AddListener(cardHandlerControl.DrawACard);    
        }
    }

    #endregion
}
