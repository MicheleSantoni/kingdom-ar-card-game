﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGenerator : MonoBehaviour
{
    #region Fields

    public List<AgentInfoSO> playerPrefabs = new List<AgentInfoSO>();
    public List<AgentInfoSO> enemyPrefabs = new List<AgentInfoSO>();
    public GameObject playersField;
    public GameObject enemiesField;
    public float agentsDistance = 4f;
    
    private GameManagement gameManagement;
    
    #endregion

    #region UnityCallbacks

    private void Awake()
    {
        gameManagement = GetComponent<GameManagement>();
        SpawnAgents();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #endregion

    #region Methods

    private void SpawnAgents()
    {
        SpawnPlayers(playerPrefabs);
        SpawnEnemies(enemyPrefabs);
    }

    private void SpawnPlayers(List<AgentInfoSO> playersInfos)
    {
        GameObject instance;
        Vector3 newPosition;
        Quaternion newRotation;
        var position = playersField.transform.position;
        float maxDistance = agentsDistance * (playersInfos.Count - 1);
        
        for (int i = 0; i < playersInfos.Count; i++)
        {
            newPosition = new Vector3(position.x + (maxDistance / 2) - (agentsDistance * i), position.y, position.z);
            instance = Instantiate(playersInfos[i].agentPrefab,newPosition, playersField.transform.rotation, playersField.transform);
            instance.GetComponent<AgentManagement>().InitStats(playersInfos[i]);
            gameManagement.players.Add(new GameManagement.Agent(playersInfos[i], instance));
        }
    }
    
    private void SpawnEnemies(List<AgentInfoSO> enemiesInfos)
    {
        GameObject instance;
        Vector3 newPosition;
        Quaternion newRotation;
        var position = enemiesField.transform.position;
        float maxDistance = agentsDistance * (enemiesInfos.Count - 1);
        
        for (int i = 0; i < enemiesInfos.Count; i++)
        {
            newPosition = new Vector3(position.x + (maxDistance / 2) - (agentsDistance * i), position.y, position.z);
            instance = Instantiate(enemiesInfos[i].agentPrefab,newPosition, enemiesField.transform.rotation, enemiesField.transform);
            instance.GetComponent<AgentManagement>().InitStats(enemiesInfos[i]);
            gameManagement.enemies.Add(new GameManagement.Agent(enemiesInfos[i], instance));
        }
    }
    
    

    #endregion
}
