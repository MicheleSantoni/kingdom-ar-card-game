﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "deck_so", menuName = "Deck", order = 1)]
public class DeckSO : ScriptableObject
{
    #region Fields
    
    [System.Serializable]
    public class Card
    {
        public Sprite cardImage;
        public enum Rarity
        {
            NORMAL,
            RARE,
            ELITE,
            ULTIMATE
        }
        public Rarity rarity;

        public enum Type
        {
            ACTION,     // can be used on player's turn against enemies/other players
            EQUIPMENT,  // can be used on player's turn; add buff/debuff effects to enemies/other players for a specific time
            COUNTER       // can be used as a counter attack to enemies actions
        }
        public Type type;
        
        public int quantity;
    }

    public string deckID;
    public Sprite playerCard;
    public Sprite heroicCard;
    public int maxCardsQuantity;
    public List<Card> cards = new List<Card>();
    
    #endregion
}
