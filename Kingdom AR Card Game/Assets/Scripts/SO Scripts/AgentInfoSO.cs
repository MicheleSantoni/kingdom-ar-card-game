﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "player_info", menuName = "Player/Info", order = 1)]
public class AgentInfoSO : ScriptableObject
{
    #region Fields

    [Title("Agent Stats")]
    public string agentID;
    public int maxHealth = 20;
    public int baseAttack = 1;
    public int baseDefense = 1;
    public int baseSpeed = 1;
    
    [Space(10)]
    
    [Title("")]
    public GameObject agentPrefab;
    public DeckSO deck;
    

    #endregion
}
