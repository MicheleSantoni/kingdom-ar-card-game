﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeCameraPosButtonControl : MonoBehaviour
{
    #region Fields
    
    public GameManagement gameManager;
    
    private Button changeCamPointButton;
    private CameraMovement camMovement;
    
    #endregion

    #region UnityCallbacks
    
    private void Start()
    {
        if (gameManager == null)
        {
            gameManager = GameObject.Find("GameManager").GetComponent<GameManagement>();
        }
        changeCamPointButton = GetComponent<Button>();

        if (gameManager)
        {
            gameManager.InitiativeRolled += SetListener;
            camMovement = gameManager.mainCam.GetComponent<CameraMovement>();
        }
    }

    #endregion
    
    #region Methods

    private void SetListener()
    {
        if (gameManager != null)
        {
            changeCamPointButton.onClick.AddListener(() => camMovement.SetNextCameraPoint(camMovement.GetNextCameraPoint()));
        }
    }
    
    
    #endregion
}
