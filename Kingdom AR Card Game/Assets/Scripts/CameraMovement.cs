﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    #region Fields

    public AnimationCurve movementCurve;
    public float maxMovementTime = 1f;

    [HideInInspector] public GameObject actualCameraPoint;
    [HideInInspector] public GameObject nextCameraPoint;
    [HideInInspector] public bool isMoving;

    private GameManagement gameManager;
    
    #endregion
    
    #region UnityCallbacks
    
    // Start is called before the first frame update
    void Start()
    {
        isMoving = false;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManagement>();
    }
    
    #endregion
    
    #region Methods

    public void SetActualCameraPoint(GameObject cameraPoint)
    {
        actualCameraPoint = cameraPoint;
    }
    
    public void SetNextCameraPoint(GameObject cameraPoint)
    {
        nextCameraPoint = cameraPoint;
        StartCoroutine(MoveTowardsNextCameraPoint());
    }
    
    public GameObject GetNextCameraPoint()
    {
        if (gameManager)
        {
            if (nextCameraPoint == null)
            {
                
                gameManager.agentTurnIndex = 0;
            }
            else
            {
                if (gameManager.agentTurnIndex < gameManager.combatOrder.Count - 1)
                {
                    gameManager.agentTurnIndex++;
                }
                else
                {
                    gameManager.agentTurnIndex = 0;
                }
            }
            return gameManager.combatOrder[gameManager.agentTurnIndex].agentObject.GetComponent<AgentManagement>().cameraPoint;
        }
        return null;
    }

    private IEnumerator MoveTowardsNextCameraPoint()
    {
        float animCurvePosition = 0f;
        float t = 0f;

        isMoving = true;
        
        while (transform.position != nextCameraPoint.transform.position)
        {
            t += Time.deltaTime;
            animCurvePosition = t / maxMovementTime;
            transform.position = Vector3.Lerp(transform.position, nextCameraPoint.transform.position, movementCurve.Evaluate(animCurvePosition));
            transform.rotation = Quaternion.Lerp(transform.rotation, nextCameraPoint.transform.rotation, movementCurve.Evaluate(animCurvePosition));
            yield return null;
        }
        
        isMoving = false;
        Debug.Log("movement done");
        SetActualCameraPoint(nextCameraPoint);
    }

    private IEnumerator RotateCameraHandler(float rotationChange)
    {
        float animCurvePosition = 0f;
        float t = 0f;

        isMoving = true;
        
        
        
        while (transform.position != nextCameraPoint.transform.position)
        {
            t += Time.deltaTime;
            animCurvePosition = t / maxMovementTime;
            transform.position = Vector3.Lerp(transform.position, nextCameraPoint.transform.position, movementCurve.Evaluate(animCurvePosition));
            transform.rotation = Quaternion.Lerp(transform.rotation, nextCameraPoint.transform.rotation, movementCurve.Evaluate(animCurvePosition));
            yield return null;
        }
        
        isMoving = false;
        Debug.Log("movement done");
    }
    
    #endregion
}
