﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartButtonControl : MonoBehaviour
{
    #region Fields
    
    public CardHandlerControl cardHandlerControl;
    
    private Button startButton;
    
    #endregion

    #region UnityCallbacks
    
    private void Start()
    {
        if (cardHandlerControl == null)
        {
            cardHandlerControl = GameObject.Find("Card Handler").GetComponent<CardHandlerControl>();
        }
        startButton = GetComponent<Button>();

        if (cardHandlerControl != null)
        {
            startButton.onClick.AddListener(cardHandlerControl.deckControl.SetDeck);   
            startButton.onClick.AddListener(DisableStartButton);
        }
    }

    #endregion
    
    #region Methods

    private void DisableStartButton()
    {
        startButton.gameObject.SetActive(false);
    }
    
    #endregion
}
