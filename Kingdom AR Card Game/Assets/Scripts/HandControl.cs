﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HandControl : MonoBehaviour
{
    #region Fields

    public List<GameObject> cardsOnHand = new List<GameObject>();
    public float horizOffset = 0.035f;
    [HideInInspector]
    public float cardDinstance;
    
    private Vector3 centralPosition;
    private Vector3 startPosition;
        
    
    #endregion
    
    #region UnityCallbacks

    private void Start()
    {
        centralPosition = Vector3.zero;
    }
    
    #endregion
    
    #region Methods
    
    public void RemoveCardToHand(int index)
    {
        cardsOnHand.RemoveAt(index);
        //Debug.Log("card removed");
    }

    public void SpacingCards()
    {
        // calculating start position
        float spacing = cardDinstance - cardsOnHand.Count * horizOffset;
        //Debug.Log("spacing " + spacing);
        startPosition = new Vector3(centralPosition.x - (spacing * (cardsOnHand.Count() - 1) / 2f), centralPosition.y, centralPosition.z);
        //Debug.Log("cards on hand " + cardsOnHand.Count);
        // spacing cards
        if (cardsOnHand.Count > 1)
        {
            for (int i = 0; i < cardsOnHand.Count; i++)
            {
                CardControl cardOnHandControl = cardsOnHand[i].GetComponent<CardControl>();
                cardsOnHand[i].transform.localPosition = startPosition + (Vector3.right * spacing * i) + (Vector3.forward * -0.07f * i);
                cardOnHandControl.SetSortingOrder(i);
                cardOnHandControl.SetTargetPosition();
            }  
        }
        else if (cardsOnHand.Count == 1)
        {
            CardControl cardOnHandControl = cardsOnHand[0].GetComponent<CardControl>();
            cardsOnHand[0].transform.localPosition = startPosition;
            cardOnHandControl.SetTargetPosition();
        }
    }
    
    #endregion
}
